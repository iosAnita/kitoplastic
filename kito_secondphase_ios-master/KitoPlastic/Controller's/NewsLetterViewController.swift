//
//  NewsLetterViewController.swift
//  KitoPlastic
//
//  Created by Apple on 05/11/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class newsLetterTableCell : UITableViewCell{
    @IBOutlet weak var TittleName: UILabel!
    @IBOutlet weak var Datelbl: UILabel!
    @IBOutlet weak var NewsImage: UIImageView!
    @IBOutlet weak var SubTittlelabel: UILabel!
   
    
    }

@available(iOS 11.0, *)
class NewsLetterViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

     @IBOutlet weak var navigationViewHeight: NSLayoutConstraint!
    @IBOutlet weak var NewsletterTableview: UITableView!
    @IBOutlet weak var nofavView: UIView!
    
    var newsData = [Any]()
    override func viewDidLoad() {
        super.viewDidLoad()
         addNavigationBarImage()
        
        NewsletterTableview.estimatedRowHeight = 49.0
        NewsletterTableview.rowHeight = UITableView.automaticDimension
        
        self.navigationController?.isNavigationBarHidden = false
        let screenSize = UIScreen.main.bounds
        if screenSize.height <= 736{
            navigationViewHeight.constant = 64
        }else{
            navigationViewHeight.constant = 88
        }
        
        
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
         GetNewsletter() //API Call
    }
    
    //MARK:- Get NewsLetter List
    func GetNewsletter(){
        self.showProgress()
        var parameter = [String:Any]()
        if SingletonClass.sharedInstance.usertype == "1"{
            parameter = ["location":SingletonClass.sharedInstance.locationId,
                         "brand":SingletonClass.sharedInstance.BrandName]
        }
        else{
            parameter = ["location_brand" : SingletonClass.sharedInstance.Newsletterarray,
                         "user_type" : SingletonClass.sharedInstance.usertype
                          ]
        }
        
        print(parameter)
        networkServices.shared.postDatawithoutHeader(methodName: methodsName.userCase.GetNewsLetter.caseValue, parameter: parameter) { (response) in
            print(response)
            self.hideProgress()
            let dic = response as! NSDictionary
            print(dic)
            if dic.value(forKey: "success") as! Bool == true {
                if (dic.value(forKey: "data") as? NSArray)!.count == 0 {
                    self.NewsletterTableview.isHidden = true
                    self.nofavView.isHidden = false
                }
                else if let data = dic.value(forKey: "data") as? NSArray{
                    self.NewsletterTableview.isHidden = false
                    self.nofavView.isHidden = true
                    // Get data value and assign in to mycompaigns array
                    self.newsData = data as! [Any]
                    print(self.newsData)
                    
                    self.NewsletterTableview.reloadData()
                }
            }
            else{
                self.NewsletterTableview.isHidden = true
                self.nofavView.isHidden = false
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let newsLetterData = newsData[indexPath.row] as! NSDictionary
        let cell = NewsletterTableview.dequeueReusableCell(withIdentifier: "newsLetterTableCell", for: indexPath) as! newsLetterTableCell
        cell.TittleName.text = (newsLetterData.value(forKey: "title")as! String)
        cell.SubTittlelabel.text = (newsLetterData.value(forKey: "message")as! String).html2String
        if (newsLetterData.value(forKey: "cover_pic") as! String == "") {
            cell.NewsImage.image = #imageLiteral(resourceName: "placeholder-image1")
        }
        else{
            // change image URL in to UIImage Type
            let profileimage = URL(string: "\(newsLetterData.value(forKey: "cover_pic") as! String)")
            cell.NewsImage.sd_setImage(with: profileimage, placeholderImage: #imageLiteral(resourceName: "placeholder-image1"))
            }
        
        if (newsLetterData.value(forKey: "created_at") as! String == ""){
            cell.Datelbl.text = ""
        }
        else{
            let myDate = (newsLetterData.value(forKey: "created_at") as! String)
//            var mystringArray = string.components(separatedBy: " ")
//            cell.Datelbl.text = mystringArray[0]
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd MMM,yyyy"
            
            let date: NSDate? = dateFormatterGet.date(from: myDate) as NSDate?
            print(dateFormatterPrint.string(from: date! as Date))
            cell.Datelbl.text = dateFormatterPrint.string(from: date! as Date)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return UITableView.automaticDimension
    }
}

//Convert HTML ode to string
extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension String {
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

