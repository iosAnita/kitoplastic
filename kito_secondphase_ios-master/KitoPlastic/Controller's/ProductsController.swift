//
//  ProductsController.swift
//  Conselho Fashion
//
//  Created by apple on 08/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import SDWebImage
@available(iOS 11.0, *)
class ProductsController: UIViewController {
    @IBOutlet weak var gridBtn: UIButton!
    @IBOutlet weak var listBtn: UIButton!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var TableViewTopConstraints: NSLayoutConstraint!
    @IBOutlet weak var productTableView: UITableView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productDescription: UILabel!
    @IBOutlet weak var productlbl: UILabel!
    @IBOutlet weak var totalProductlbl: UILabel!
    @IBOutlet weak var filterBtn: UIButton!
    @IBOutlet weak var searchTF: UITextField!
    
    var tintView = UIView()
    var isSearchActive = "no"
    var isButtonSelected = "list"
    var CompaignDetailData = [Any]()
    var mylanguageCode = "pt-BR"
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        searchView.isHidden = true
        TableViewTopConstraints.constant = 5
        listBtn.setImage(#imageLiteral(resourceName: "listViewYellow"), for: .normal)
        gridBtn.setImage(#imageLiteral(resourceName: "girdWhite"), for: .normal)
        getCompaignDetail() // GetcompaignDetail Api call
    }
    
    override func viewWillAppear(_ animated: Bool) {
        languageChange()
        self.navigationController?.isNavigationBarHidden = true
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    //MARK:- language change function
    func languageChange(){
        productlbl.text = "Products".localized(loc: mylanguageCode)
       // filterBtn.setTitle("Filter".localized(loc: mylanguageCode), for: .normal)
        searchTF.placeholder = "SearchProducts".localized(loc: mylanguageCode)
     }
    
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func searchbtn(_ sender: UIButton) {
        if isSearchActive == "no"{
            UIView.animate(withDuration: 0.3, delay: 0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                self.searchView.isHidden = false
                self.TableViewTopConstraints.constant = 50
                self.isSearchActive = "yes"
                self.view.layoutIfNeeded()
                
            }, completion: nil)
            
        }else{
            UIView.animate(withDuration: 0.3, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                self.searchView.isHidden = true
                self.TableViewTopConstraints.constant = 5
                self.isSearchActive = "no"
                self.view.layoutIfNeeded()
                
            }, completion: nil)
            
        }
    }
    @IBAction func listViewBtn(_ sender: UIButton) {
        listBtn.setImage(#imageLiteral(resourceName: "listViewYellow"), for: .normal)
        gridBtn.setImage(#imageLiteral(resourceName: "girdWhite"), for: .normal)
        isButtonSelected = "list"
        productTableView.reloadData()
    }
    @IBAction func gridViewBtn(_ sender: UIButton) {
        listBtn.setImage(#imageLiteral(resourceName: "listViewWhite"), for: .normal)
        gridBtn.setImage(#imageLiteral(resourceName: "gridYellow"), for: .normal)
        isButtonSelected = "grid"
        productTableView.reloadData()
    }
    
}
@available(iOS 11.0, *)
extension ProductsController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isButtonSelected == "list"{
            return CompaignDetailData.count
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isButtonSelected == "list"{
            if let cell1 = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as? productListCell1{
                // get data from Compaign detail data and put in detailView
                let myDetailData = CompaignDetailData[indexPath.row]  as! NSDictionary
                // get product name
                if (myDetailData.value(forKey: "product_name") != nil){
                    cell1.product_name.text = (myDetailData.value(forKey: "product_name") as! String)
                }
                else{
                    print("my Detail data does'nt contain product name")
                }
                // get product brand
                if (myDetailData.value(forKey: "product_brand") != nil)
                {
                    cell1.product_Brand.text = (myDetailData.value(forKey: "product_brand") as! String)
                }
                else{
                    print("my Detail data does'nt contain product brand")
                }
                // get point per sale
                if (myDetailData.value(forKey: "points_per_sale") != nil){
                    let String1 = (myDetailData.value(forKey: "points_per_sale") as! String)
                    let string2 = "pointsnumber".localized(loc: mylanguageCode)
                    let string3 = ("\(string2)" + " " + "\(String1)")
                    cell1.point_perSale.text = string3
                }
                else{
                    print("my Detail data does'nt contain product point per sale")
                }
                //get image
                if (myDetailData.value(forKey: "product_image") as! String == "") {
                    cell1.productImg.image = #imageLiteral(resourceName: "placeholder-image1")
                }
                else{
                    
                    // change image URL in to UIImage Type
                    let profileimage = URL(string: "\(myDetailData.value(forKey: "product_image") as! String)")
                    cell1.productImg.sd_setImage(with: profileimage, placeholderImage: #imageLiteral(resourceName: "placeholder-image1"))
                }
                cell1.soldLbl.text = "Sold".localized(loc: mylanguageCode)
               
                
                if indexPath.row % 2 == 0{
                    cell1.soldLbl.isHidden = true
                }else{
                    cell1.soldLbl.isHidden = true
                    }
                return cell1
            }
        }else{
            if let cell2 = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as? productListCell2{
                return cell2
            }
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isButtonSelected == "list"{
            return 100
        }else{
            return productTableView.frame.size.height
        }
    }
    //MARK:- GetCompaign API Fuction
    func getCompaignDetail(){
        self.showProgress()
        let parameter = [ "campaign_id": SingletonClass.sharedInstance.myCompaignId]
        networkServices.shared.postDatawithHeader(methodName: methodsName.userCase.GetCompaignDetails.caseValue, parameter: parameter) { (response) in
            print(response)
            self.hideProgress()
            let dic = response as! NSDictionary
            print(dic)
            if dic.value(forKey: "success") as! Bool == true
            {
                if let data = dic.value(forKey: "data") as? NSDictionary{
                print(data)
                    let totalProduct = "\(data.value(forKey: "totalproduct") as! NSNumber)"
                    self.totalProductlbl.text = "\(totalProduct) Products"
                    // get data nd assign in to CompaignDetail data
                   // self.CompaignDetailData = [data]
                    if let compaign = data.value(forKey: "campaign") as? NSDictionary{
                    print(compaign)
                        self.productName.text = (compaign.value(forKey: "name") as! String)
                        self.productDescription.text = (compaign.value(forKey: "description") as! String)
                        }
                    if let product_detail = data.value(forKey: "product_details") as? NSArray{
                        self.CompaignDetailData = product_detail as! [Any]
                        print(self.CompaignDetailData)
                      self.productTableView.reloadData()
                        
                    }
                    }
                
                }
            else{
                self.hideProgress()
                self.ShowAlertView(title: "Soamer", message: dic.value(forKey: "message") as! String, viewController: self)
            }
            }
    }
    
}
@available(iOS 11.0, *)
extension ProductsController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return CompaignDetailData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath as IndexPath) as? productGridCell
        {
            // get data from Compaign detail data and put in detailView
            let myDetailData = CompaignDetailData[indexPath.row]  as! NSDictionary
            // get product name
            if (myDetailData.value(forKey: "product_name") != nil){
                cell.GProductName.text = (myDetailData.value(forKey: "product_name") as! String)
            }
            else{
                print("my Detail data does'nt contain product name")
            }
            // get product brand
            if (myDetailData.value(forKey: "product_brand") != nil)
            {
                cell.GproductBrand.text = (myDetailData.value(forKey: "product_brand") as! String)
            }
            else{
                print("my Detail data does'nt contain product brand")
            }
            // get point per sale
            if (myDetailData.value(forKey: "points_per_sale") != nil){
                let String1 = (myDetailData.value(forKey: "points_per_sale") as! String)
                let string2 = "pointsnumber".localized(loc: mylanguageCode)
                let string3 = ("\(string2)" + " " + "\(String1)")
                cell.GPointSale.text = string3
            }
            else{
                print("my Detail data does'nt contain product point per sale")
            }
            //get image
            if (myDetailData.value(forKey: "product_image") as! String == "") {
                cell.GProduct_img.image = #imageLiteral(resourceName: "placeholder-image1")
            }
            else{
                  // change image URL in to UIImage Type
                let profileimage = URL(string: "\(myDetailData.value(forKey: "product_image") as! String)")
                cell.GProduct_img.sd_setImage(with: profileimage, placeholderImage: #imageLiteral(resourceName: "placeholder-image1"))
            }
           
            return cell
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         return CGSize(width:(collectionView.frame.width)/2, height: 200)
     }
    
}
class productListCell1:UITableViewCell{
    @IBOutlet weak var soldLbl: UILabel!
    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var product_name: UILabel!
    @IBOutlet weak var product_Brand: UILabel!
    @IBOutlet weak var point_perSale: UILabel!
    
}
class productListCell2:UITableViewCell{
    
   }

class productGridCell:UICollectionViewCell{
    @IBOutlet weak var GProduct_img: UIImageView!
    @IBOutlet weak var GProductName: UILabel!
    @IBOutlet weak var GproductBrand: UILabel!
    @IBOutlet weak var GPointSale: UILabel!
    }
