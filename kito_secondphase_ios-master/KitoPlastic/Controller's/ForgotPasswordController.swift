//
//  ForgotPasswordController.swift
//  Conselho Fashion
//
//  Created by apple on 06/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

@available(iOS 11.0, *)
class ForgotPasswordController: UIViewController {
    @IBOutlet weak var eMailTF: UITextField!
    @IBOutlet weak var forgotpasswordText: UILabel!
    @IBOutlet weak var entermailtext: UILabel!
    @IBOutlet weak var emailText: UILabel!
    @IBOutlet weak var sendpasswordBtn: UIButton!
    @IBOutlet weak var cloaseBtn: UIButton!
    var mylanguageCode = "pt-BR"
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        changeStoryboardWithLanguage()
    }
    //MARK:- language change
    func changeStoryboardWithLanguage(){
        forgotpasswordText.text = "Forgotpassword".localized(loc: mylanguageCode)
        entermailtext.text = "ForgotMessage".localized(loc: mylanguageCode)
        emailText.text = "Email".localized(loc: mylanguageCode)
        eMailTF.placeholder = "Email".localized(loc: mylanguageCode)
        sendpasswordBtn.setTitle("SendPassword".localized(loc: mylanguageCode), for: .normal)
        cloaseBtn.setTitle("Close".localized(loc: mylanguageCode), for: .normal)
    }
    
    @IBAction func Close(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    @IBAction func sendBtnTouch(_ sender: UIButton) {
        self.view.endEditing(true)
        if (eMailTF.text == ""){
            ShowAlertView(title: "Soamer", message: "Please enter valid eMail", viewController: self)
        }
            
        else if (isValidEmail(testStr: eMailTF.text!) == false){
            ShowAlertView(title: "Soamer", message: "Email must be in valid form", viewController: self)
        }
        else{
            forgetPasswordApi()
        }
    }
    //MARK: Email Format Validation
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    // MARK:-  forget password Data on Server With Alarmofire
    func forgetPasswordApi(){
        showProgress()
        let parameter = ["email" : eMailTF.text!]
        networkServices.shared.postDatawithoutHeader(methodName: methodsName.userCase.forgetPassword.caseValue, parameter: parameter) { (response) in
            print(response)
            let dic = response as! NSDictionary
            print(dic)
            if dic.value(forKey: "success") as!Bool == true{
                self.hideProgress()
                // show alert and go to Login page
                let alertController = UIAlertController(title: "Soamer", message: dic.value(forKey: "message") as! String, preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    self.navigationController?.popViewController(animated: true)
                }
                // Add the actions
                alertController.addAction(okAction)
                // Present the controller
                self.present(alertController, animated: true, completion: nil)
            }
            else {
                self.hideProgress()
                self.ShowAlertView(title: "Soamer", message: dic.value(forKey: "message") as! String, viewController: self)
            }
        }
    }
}
