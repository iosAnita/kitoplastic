//
//  WebViewController.swift
//  KitoPlastic
//
//  Created by Pankaj Rana on 23/05/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import WebKit
class WebViewController: UIViewController,WKNavigationDelegate {

    @IBOutlet weak var webVw: WKWebView!
    var titleName = ""
    var open_link = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = titleName
        addNavigationBarImage()
        self.navigationController?.isNavigationBarHidden = false
        let url = URL(string: open_link)!
        webVw.load(URLRequest(url: url))
        webVw.allowsBackForwardNavigationGestures = true
        // Do any additional setup after loading the view.
    }
    


}
