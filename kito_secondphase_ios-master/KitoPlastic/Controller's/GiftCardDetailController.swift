//
//  GiftCardDetailController.swift
//  Conselho Fashion
//
//  Created by apple on 07/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

@available(iOS 11.0, *)
class GiftCardDetailController: UIViewController {
@IBOutlet weak var NavigationViewHeight: NSLayoutConstraint!
    @IBOutlet weak var popUpView: DesignableView!
    @IBOutlet weak var hiddenView: UIView!
    @IBOutlet weak var GiftImage: UIImageView!
    @IBOutlet weak var Gifttittle: UILabel!
    @IBOutlet weak var GiftRange: UILabel!
    @IBOutlet weak var Validtime: UILabel!
    @IBOutlet weak var RedemptionTF: UITextField!
    @IBOutlet weak var descriptionlbl: UILabel!
    @IBOutlet weak var choosevalueForRedeem: UILabel!
    @IBOutlet weak var reddemNoeBtn: UIButton!
    
    
    
    var myVoucherid : String = ""
    var myGiftDetail = SingletonClass.sharedInstance.GiftVouncherDetail
    var mylanguageCode = "pt-BR"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getGiftDetail()  //]function Call
        self.title = "GiftCards".localized(loc: mylanguageCode)
        addNavigationBarImage()
        self.navigationController?.isNavigationBarHidden = false
        // Do any additional setup after loading the view.
        hiddenView.isHidden = true
        popUpView.isHidden = true
        self.hiddenView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTappedOnView)))
    }
    override func viewWillAppear(_ animated: Bool) {
        languageChange()
    }
    //language change function
    func languageChange(){
        reddemNoeBtn.setTitle("RedeemNow".localized(loc: mylanguageCode), for: .normal)
        choosevalueForRedeem.text = "ChooseValue".localized(loc: mylanguageCode)
    }
    func getGiftDetail(){
        // change image URL in to UIImage Type
        let profileimage = URL(string: "\(myGiftDetail.value(forKey: "image") as! String)")
        GiftImage.sd_setImage(with: profileimage, placeholderImage: #imageLiteral(resourceName: "placeholder-image1"))
        Gifttittle.text = (myGiftDetail.value(forKey: "title") as! String)
        GiftRange.text = (myGiftDetail.value(forKey: "price_range") as! String)
        Validtime.text = (myGiftDetail.value(forKey: "validity_period") as! String)
        descriptionlbl.text = (myGiftDetail.value(forKey: "description") as! String)
        myVoucherid = "\(myGiftDetail.value(forKey: "id")as! NSNumber)"
        }
    
     @objc func didTappedOnView(){
        popUpView.isHidden = true
        hiddenView.isHidden = true
    }
    @IBAction func redeemNow(_ sender: UIButton) {
        
        if RedemptionTF.text == ""{
            self.ShowAlertView(title: "Soamer", message: "RedeemAlert".localized(loc: mylanguageCode), viewController: self)
        }
        else{
           RedeemGiftVoucher()
        }
}
    //MARK:- Redeem Fift Voucher API
    func RedeemGiftVoucher(){
        self.showProgress()
        let parameter = [
            "voucher_id" : myVoucherid,
            "requested_points" : RedemptionTF.text!
        ]
        print(parameter)
        networkServices.shared.postDatawithHeader(methodName: methodsName.userCase.RedeemGiftVoucher.caseValue, parameter: parameter) { (response) in
            print(response)
            self.hideProgress()
            let dic = response as! NSDictionary
            if dic.value(forKey: "success") as! Bool == true{
                // Show alert View
                let alertController = UIAlertController(title: "Soamer", message: (dic.value(forKey: "message") as! String), preferredStyle: .alert)
                let acceptAction = UIAlertAction(title: "OK", style: .default) { (_) -> Void in
                    self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(acceptAction)
                self.present(alertController, animated: true, completion: nil)
              }
            else{
                // Show alert View
                let alertController = UIAlertController(title: "Soamer", message: (dic.value(forKey: "message") as! String), preferredStyle: .alert)
                let acceptAction = UIAlertAction(title: "OK", style: .default) { (_) -> Void in
                    self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(acceptAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
}
