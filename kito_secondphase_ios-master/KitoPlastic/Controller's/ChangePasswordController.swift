//
//  ChangePasswordController.swift
//  Conselho Fashion
//
//  Created by apple on 08/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

@available(iOS 11.0, *)
class ChangePasswordController: UIViewController {
    @IBOutlet weak var oldPasswordTF: UITextField!
    @IBOutlet weak var newPasswordTF: UITextField!
    @IBOutlet weak var confirmPasswordTF: UITextField!
    @IBOutlet weak var oldPasswordlbl: UILabel!
    @IBOutlet weak var newPasswordlbl: UILabel!
    @IBOutlet weak var confirmPasswordlbl: UILabel!
    @IBOutlet weak var updateBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    
    var mylanguageCode = "pt-BR"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "ChangePassword".localized(loc: mylanguageCode)
        addNavigationBarImage()
        self.navigationController?.isNavigationBarHidden = false
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        languageChange()
    }
    //MARK:- language change
    func languageChange(){
        oldPasswordlbl.text = "OldPassword".localized(loc: mylanguageCode)
        newPasswordlbl.text = "NewPassword".localized(loc: mylanguageCode)
        confirmPasswordlbl.text = "ConfirmPassword".localized(loc: mylanguageCode)
        updateBtn.setTitle("Update".localized(loc: mylanguageCode), for: .normal)
        cancelBtn.setTitle("Cancel".localized(loc: mylanguageCode), for: .normal)
    }
    
    @IBAction func updateTap(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if oldPasswordTF.text == ""{
            ShowAlertView(title: "Soamer", message: "oldpasswordAlert".localized(loc: mylanguageCode), viewController: self)
        }
       else if newPasswordTF.text == ""{
            ShowAlertView(title: "Soamer", message: "NewpasswordAlert".localized(loc: mylanguageCode), viewController: self)
        }
        else if confirmPasswordTF.text == ""{
            ShowAlertView(title: "Soamer", message: "ConfirmPasswordalert".localized(loc: mylanguageCode), viewController: self)
        }
        else if ((newPasswordTF.text?.count)! <= 6){
            ShowAlertView(title: "Soamer", message: "PasswordGreaterAlert".localized(loc: mylanguageCode), viewController: self)
        }
        else if (confirmPasswordTF.text != newPasswordTF.text){
            ShowAlertView(title: "Soamer", message: "EqualPassword".localized(loc: mylanguageCode), viewController: self)
        }
        else{
        changePasword()
        
        }
    }
    
    @IBAction func CancelTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // change password API
    func changePasword(){
        self.showProgress()
        let parameter = [
            "old_password" : oldPasswordTF.text!,
            "new_password" : newPasswordTF.text!,
            "confirm_password" : confirmPasswordTF.text!
           ]
        
        networkServices.shared.postDatawithHeader(methodName: methodsName.userCase.changePassword.caseValue, parameter: parameter) { (response) in
            print(response)
            let dic = response as! NSDictionary
            print (dic)
            if dic.value(forKey: "success") as! Bool == true
            {
                self.hideProgress()
                // show alert and go to Setting page
                let alertController = UIAlertController(title: "Soamer", message: "passwordChanged".localized(loc: self.mylanguageCode) , preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    self.navigationController?.popViewController(animated: true)
                }
                // Add the actions
                alertController.addAction(okAction)
                // Present the controller
                self.present(alertController, animated: true, completion: nil)
                }
            else{
                self.hideProgress()
                self.ShowAlertView(title: "Soamer", message: dic.value(forKey: "message") as! String, viewController: self)
                }
        }
    }
}
