//
//  RegisterSaleController.swift
//  Conselho Fashion
//
//  Created by apple on 08/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

@available(iOS 11.0, *)
class RegisterSaleController: UIViewController,QRScannerViewDelegate{
    
    @IBOutlet weak var QRcodeTF: UITextField!
    @IBOutlet weak var placetheCodelbl: UILabel!
    @IBOutlet weak var orlbl: UILabel!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var scannerview: QRScannerView!
    
   // var codeResult: LBXScanResult?
    var ScanResultQRCode : String = ""
    var CompaignID = SingletonClass.sharedInstance.myCompaignId
    var mylanguageCode = "pt-BR"
    
    var qrData: QRData? = nil {
        didSet {
            if qrData != nil {
                 ScanResultQRCode = qrData!.codeString
                  print(ScanResultQRCode)
                RegisterSalesWithQR() //API Call
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scannerview.delegate = self
        self.title = "Register".localized(loc: mylanguageCode)
        addNavigationBarImage()
        self.navigationController?.isNavigationBarHidden = false
         self.navigationController?.navigationBar.barTintColor = UIColor.darkGray
       
        // Do any additional setup after loading the view.
       }
    override func viewWillAppear(_ animated: Bool) {
        languageChange()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    //MARK:- language Change
    func languageChange(){
       placetheCodelbl.text = "PlaceCode".localized(loc: mylanguageCode)
        orlbl.text = "or".localized(loc: mylanguageCode)
        submitBtn.setTitle("Submit".localized(loc: mylanguageCode), for: .normal)
        QRcodeTF.placeholder = "Writemanually".localized(loc: mylanguageCode)
        
    }
    
 
   
   

    
    @IBAction func QRCodeTap(_ sender: UIButton) {
       // openScanner()
    }
    
    @IBAction func submitTap(_ sender: UIButton) {
        if QRcodeTF.text == ""{
            self.ShowAlertView(title: "Soamer", message: "QRalert".localized(loc: mylanguageCode), viewController: self)
        }
        else{
            ScanResultQRCode = QRcodeTF.text!
            RegisterSalesWithQR()  //Api Call
        }
    }
    
    
    // Register Sales using QRCode
    func RegisterSalesWithQR(){
        let trimStrm = ScanResultQRCode.trimmingCharacters(in: CharacterSet(charactersIn: "\""))
        let parameter: [String: Any] = [
            "qr_code": trimStrm,
            "campaign_id": CompaignID,
            "location" : SingletonClass.sharedInstance.locationId
        ]
          print(parameter)
        networkServices.shared.postDatawithHeader(methodName: methodsName.userCase.ClaimPointsUsingQrcodes.caseValue, parameter: parameter) { (response) in
            print(response)
            let dic = response as! NSDictionary
            if dic.value(forKey: "success") as! Bool == true{
                // Show alert View
                let alertController = UIAlertController(title: "Soamer", message: dic.value(forKey: "message") as! String, preferredStyle: .alert)
                let acceptAction = UIAlertAction(title: "OK", style: .default) { (_) -> Void in
                    self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(acceptAction)
                self.present(alertController, animated: true, completion: nil)
            }
            else{
                // Show alert View
                let alertController = UIAlertController(title: "Soamer", message: dic.value(forKey: "message") as! String, preferredStyle: .alert)
                let acceptAction = UIAlertAction(title: "OK", style: .default) { (_) -> Void in
                    self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(acceptAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    //QRCode delegates
    func qrScanningDidFail() {
        ShowAlertView(title: "Error", message: "QRError".localized(loc: mylanguageCode), viewController: self)
    }
    
    func qrScanningSucceededWithCode(_ str: String?) {
        self.qrData = QRData(codeString: str!)
    }
    
    func qrScanningDidStop() {
        print("Scanner have stooped")
    }
    
}
