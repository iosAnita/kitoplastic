//
//  CompaginController.swift
//  KitoPlastic
//
//  Created by apple on 06/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import SDWebImage
@available(iOS 11.0, *)
class CompaginController: UIViewController {
    var selectedcell = -1
    var cell1 = compaignCell()
    var mycompaignData = [Any]()
    @IBOutlet weak var NavigationViewHeight: NSLayoutConstraint!
    @IBOutlet weak var compaigntableview: UITableView!
    @IBOutlet weak var noFavouriteView: UIView!
    @IBOutlet weak var campaignslbl: UILabel!
    @IBOutlet weak var noCampaignlbl: UILabel!
    var mylanguageCode = "pt-BR"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        noFavouriteView.isHidden = true
        // Do any additional setup after loading the view.
        addNavigationBarImage()
        self.navigationController?.isNavigationBarHidden = false
        let screenSize = UIScreen.main.bounds
        if screenSize.height <= 736{
            NavigationViewHeight.constant = 64
        }else{
            NavigationViewHeight.constant = 88
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        languageChange()
        GetCompaigns()   //Get compaigns Api function call
    }
    
    //MARK:- language change function
    func languageChange(){
        campaignslbl.text = "Campaigns".localized(loc: mylanguageCode)
        noCampaignlbl.text = "CampaignMessage".localized(loc: mylanguageCode)
    }
    
    @IBAction func RegisterSalesTap(_ sender: UIButton) {
        let selectedButton = sender.tag
        print("you have selected cell No: \(selectedButton)")
        let CompaignDetail = mycompaignData[selectedButton] as! NSDictionary
        let myCompaign_id = CompaignDetail.value(forKey: "id") as! NSNumber
        SingletonClass.sharedInstance.myCompaignId = "\(myCompaign_id)"
        print("my compaign id is:\(myCompaign_id)")
        
       let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        let a = CompaignDetail.value(forKey: "form_source") as! String
        let b = a.components(separatedBy: "?")
        let c = b[0]
        vc.open_link =  c + "?user_id=" + "\(UserDefaults.standard.value(forKey: "userid") as! String)"
        vc.titleName =  CompaignDetail.value(forKey: "name") as! String
        self.navigationController?.pushViewController(vc, animated: true)
        
//        performPushSeguefromController(identifier: "WebViewController")
    }
    
    //Mark:- Get Compaigns API
    func GetCompaigns(){
        showProgress()
        var location = ""
        if SingletonClass.sharedInstance.usertype == "1"{
            location = SingletonClass.sharedInstance.locationId
        }
        else{
            location = SingletonClass.sharedInstance.locationIDArray.joined(separator: ",")
        }
        let parameter = ["location" : location]
        print(parameter)
        networkServices.shared.postDatawithHeader(methodName: methodsName.userCase.GetCompaigns.caseValue, parameter: parameter) { (response) in
            print(response)
            self.hideProgress()
            let dic = response as! NSDictionary
            print(dic)
            if dic.value(forKey: "success") as! Bool == true {
                if (dic.value(forKey: "data") as? NSArray)!.count == 0 {
                    self.compaigntableview.isHidden = true
                    self.noFavouriteView.isHidden = false
                }
                else if let data = dic.value(forKey: "data") as? NSArray{
                    self.compaigntableview.isHidden = false
                    self.noFavouriteView.isHidden = true
                    // Get data value and assign in to mycompaigns array
                    self.mycompaignData = data as! [Any]
                    print(self.mycompaignData)
                    self.compaigntableview.reloadData()
                }
            }
            else{
                self.compaigntableview.isHidden = true
                self.noFavouriteView.isHidden = false
                self.hideProgress()
            }
        }
    }
}
@available(iOS 11.0, *)
extension CompaginController:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mycompaignData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let compaigndata = mycompaignData[indexPath.row] as! NSDictionary
        cell1 = tableView.dequeueReusableCell(withIdentifier: "cell") as! compaignCell
        cell1.nameText.text = "Name".localized(loc: mylanguageCode)
        cell1.startOnText.text = "startOn".localized(loc: mylanguageCode)
        cell1.EndonText.text = "Endon".localized(loc: mylanguageCode)
        cell1.RegisterBtn.setTitle("RegisterSale".localized(loc: mylanguageCode), for: .normal)
        // get value from compaigndata and assign in to varaible
        
        if (compaigndata.value(forKey: "image") as! String == "") {
            cell1.compaignsImage.image = #imageLiteral(resourceName: "placeholder-image1")
        }
        else{
            // change image URL in to UIImage Type
            let profileimage = URL(string: "\(compaigndata.value(forKey: "image") as! String)")
            cell1.compaignsImage.sd_setImage(with: profileimage, placeholderImage: #imageLiteral(resourceName: "placeholder-image1"))
            
        }
        if (compaigndata.value(forKey: "start_date") != nil) {
            cell1.startDate.text = (compaigndata.value(forKey: "start_date") as! String)
        }
        else{
            print("Compaign data have not any start date")
        }
        if (compaigndata.value(forKey: "end_date") != nil){
            cell1.enddate.text = (compaigndata.value(forKey: "end_date") as! String)
        }
        else{
            print("Compaign data have not any End date")
        }
        if (compaigndata.value(forKey: "name") != nil) {
            cell1.nameLbl.text = (compaigndata.value(forKey: "name") as! String)
        }
        else{
            print("Compaign data have not any name")
        }
        if SingletonClass.sharedInstance.usertype == "1"{
            //cell1.RegisterBtn.isHidden = true
            cell1.RegisterBtn.tag = indexPath.row
        }
        else{
            //cell1.RegisterBtn.isHidden = true
        }
        
//        if (compaigndata.value(forKey: "submitted") as! String == "Yes") {
//            cell1.RegisterBtn.isHidden = true
//        }
//        else{
//           cell1.RegisterBtn.isHidden = true
//        }
//
        return cell1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if SingletonClass.sharedInstance.usertype == "1"{
            return 320
        }
        else{
            return 280
        }
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //cell1.bgView.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let compaigndata1 = mycompaignData[indexPath.row] as! NSDictionary
        selectedcell = indexPath.row
        print("you have selected cell No: \(selectedcell)")
        let myCompaign_id = compaigndata1.value(forKey: "id") as! Int
        SingletonClass.sharedInstance.myCompaignId = "\(myCompaign_id)"
        print("my compaign id is:\(myCompaign_id)")
    }
}
class compaignCell:UITableViewCell{
    @IBOutlet weak var bgView: DesignableView!
    @IBOutlet weak var compaignsImage: DesignableImage!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var enddate: UILabel!
    @IBOutlet weak var RegisterBtn: UIButton!
    @IBOutlet weak var nameText: UILabel!
    @IBOutlet weak var startOnText: UILabel!
    @IBOutlet weak var EndonText: UILabel!
}
