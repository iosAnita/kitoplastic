//
//  AllCompaignViewController.swift
//  Conselho Fashion
//
//  Created by Apple on 19/09/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

@available(iOS 11.0, *)
class AllCompaignViewController: UIViewController,LBZSpinnerDelegate{
    
    

    @IBOutlet weak var compaignView: LBZSpinner!
    @IBOutlet weak var navigationViewHeight: NSLayoutConstraint!
    @IBOutlet weak var campaigns: UILabel!
    @IBOutlet weak var registerSaleBtn: UIButton!
    @IBOutlet weak var selectCampaignslbl: UILabel!
    @IBOutlet weak var nofavView: UIView!
    
    
    var CompaignArray : [String] = []
    var CompaignIDArray : [String] = []
    var Compaignid : String = ""
    
    func spinnerChoose(_ spinner: LBZSpinner, index: Int, value: String) {
        var spinnerName = ""
        if spinner == compaignView { spinnerName = "Escolha um Compaign" }
        if spinner == compaignView {
            print("Spinner : \(spinnerName) : { Index : \(index) - \(value) }")
           
            Compaignid = CompaignIDArray[index]
            SingletonClass.sharedInstance.myCompaignId = (Compaignid)
            compaignView.text = value
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        nofavView.isHidden = true
          GetCompaigns()   //Get compaigns Api function call
        compaignView.lineColor = .clear
        
       // addNavigationBarImage()
       // self.navigationController?.isNavigationBarHidden = false
        let screenSize = UIScreen.main.bounds
        if screenSize.height <= 736{
            navigationViewHeight.constant = 64
        }else{
            navigationViewHeight.constant = 88
        }
        
        // Do any additional setup after loading the view.
    }
  
    override func viewWillAppear(_ animated: Bool) {
         self.navigationController?.isNavigationBarHidden = true
      
        compaignView.delegate = self
        if compaignView.selectedIndex == LBZSpinner.INDEX_NOTHING {
            print("NOTHING VALUE")
            compaignView.text = "Escolha um Missões"
        }
    }
    @IBAction func RegisterTap(_ sender: UIButton) {
        if compaignView.text == "Escolha um Compaign"{
            self.ShowAlertView(title: "Soamer", message: "Por favor, selecione primeiro", viewController: self)
        }
        else{
            print("Your Selected Compaign id is :- \(Compaignid)")
               performPushSeguefromController(identifier: "RegisterSaleController")
        }
       
    }
    
    @IBAction func BackTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //Mark:- Get Compaigns API
    func GetCompaigns(){
        showProgress()
        let parameter = ["location" : SingletonClass.sharedInstance.locationId]
                        
        networkServices.shared.postDatawithHeader(methodName: methodsName.userCase.GetCompaigns.caseValue, parameter: parameter) { (response) in
            print(response)
            let dic = response as! NSDictionary
            print(dic)
            self.hideProgress()
            if dic.value(forKey: "success") as! Bool == true {
                if (dic.value(forKey: "data") as? NSArray)!.count == 0 {
                     self.nofavView.isHidden = false
                }
               
              else if let data = dic.value(forKey: "data") as? NSArray{
                   self.nofavView.isHidden = true
                    // Get data value and assign in to mycompaigns array
                   let mycompaignData = data as! [Any]
                      for i in 0..<mycompaignData.count{
                        self.CompaignArray.append((data[i] as AnyObject).value(forKey: "name") as! String)
                        self.CompaignIDArray.append("\((data[i]as AnyObject).value(forKey: "id") as! NSNumber)")
                    }
                    
                    self.compaignView.updateList(self.CompaignArray)
                 }
            }
            else{
                self.hideProgress()
                self.ShowAlertView(title: "Soamer", message: dic.value(forKey: "message") as! String, viewController: self)
            }
        }
        
    }

}
