//
//  SingletonClass.swift
//  KeepUp
//
//  Created by SIERRA on 31/01/18.
//  Copyright © 2018 MAC. All rights reserved.
//

import UIKit

class SingletonClass: NSObject {
    // MARK: Singleton Object Creation
    static let sharedInstance: SingletonClass = {
        let singletonObject = SingletonClass()
        return singletonObject
     }()
    var scanBarCode = String()
    var locationId = ""
    var myCompaignId = String()
    var GiftVouncherDetail : NSDictionary = [:]
    var notifactionTap = ""
    var BrandName = ""
    
//    var LocationDict = ["Brand":"",
//                        "State":"",
//                        "City":"",
//                        "Location":"",
//                        "LocationID":""]
//    var LocationArray = [[String:String]]()
    var locationIDArray = [String]()
    var usertype = ""
    var NewsletterDict = ["Brand":"",
                          "LocationID":""]
    var Newsletterarray = [[String:String]]()
}
